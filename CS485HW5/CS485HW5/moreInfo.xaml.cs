﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS485HW5
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MoreInfo : ContentPage
    {
        public MoreInfo()
        {
            InitializeComponent();
        }

        public MoreInfo(ProductInfo productInfo)
        {
            InitializeComponent();
            BindingContext = productInfo;
        }
    }
}