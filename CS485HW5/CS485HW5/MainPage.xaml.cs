﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using CS485HW5;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Xamarin.Forms.Xaml;

namespace CS485HW5
{
    public partial class MainPage : ContentPage
    {
        public List<ProductInfo> productList;

        public MainPage()
        {
            InitializeComponent();
            ReadJson();
        }

        async void MenuItem_Clicked(object sender, EventArgs e)
        {
            var item = (MenuItem)sender;
            var itemSelected = item.CommandParameter as ProductInfo;
            await Navigation.PushAsync(new MoreInfo(itemSelected));

        }
        private void ReadJson()
        {
            var FileName = "CS485HW5.products.json";

            var assembly = typeof(MainPage).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(FileName);

            ProductInfo data;
            using (var reader = new System.IO.StreamReader(stream))
            {
                var jsonAsString = reader.ReadToEnd();

                productList = JsonConvert.DeserializeObject<List<ProductInfo>>(jsonAsString);

            }
            productListView.ItemsSource = productList;


        }
    }

}