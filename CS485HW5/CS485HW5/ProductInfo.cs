﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using CS485HW5;
namespace CS485HW5
{
    public partial class ProductInfo
    {


        public long productId
        {
            get;
            set;
        }


        public string productName
        {
            get;
            set;
        }

        public string productCode
        {
            get;
            set;
        }

        public string releaseDate
        {
            get;
            set;
        }

        public string description
        {
            get;
            set;
        }

        public double price
        {
            get;
            set;
        }

        public double starRating
        {
            get;
            set;
        }

        public Uri imageUrl
        {
            get;
            set;
        }

        public string MoreInformation
        {
            get;
            set;
        }


    }
}
